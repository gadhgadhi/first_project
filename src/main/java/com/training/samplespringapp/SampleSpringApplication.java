package com.training.samplespringapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;

@SpringBootApplication
public class SampleSpringApplication {

	@GetMapping(value={"", "/"})
	public String get() {
		return "index";
	}

	public static void main(String[] args) {
		SpringApplication.run(SampleSpringApplication.class, args);
	}

}


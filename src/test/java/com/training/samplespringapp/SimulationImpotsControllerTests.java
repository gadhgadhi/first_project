package com.training.samplespringapp;

import org.junit.Test;
import org.junit.runner.RunWith;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

import com.training.samplespringapp.mvc.SimulationImpotsController;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SimulationImpotsControllerTests {

	@Test
	public void testCalculNombreParts() {
		SimulationImpotsController controller = new SimulationImpotsController();
		assertThat(controller.calculNombreParts(false, 0)).isEqualTo(1);
		assertThat(controller.calculNombreParts(false, 1)).isEqualTo(1.5);
		assertThat(controller.calculNombreParts(false, 3)).isEqualTo(3);
		assertThat(controller.calculNombreParts(true, 0)).isEqualTo(2);
		assertThat(controller.calculNombreParts(true, 1)).isEqualTo(2.5);
		assertThat(controller.calculNombreParts(true, 3)).isEqualTo(4);
	}

	@Test
	public void testCalculImpotRevenu() {
		SimulationImpotsController controller = new SimulationImpotsController();
		assertThat(controller.calculImpotRevenu(35000, 1.5)).isEqualTo(2842);
		assertThat(controller.calculImpotRevenu(45000, 2)).isEqualTo(3556);
		assertThat(controller.calculImpotRevenu(75000, 2)).isEqualTo(11116);
	}

}

